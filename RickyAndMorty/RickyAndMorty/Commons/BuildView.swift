//
//  BuildView.swift
//  RickyAndMorty
//
//  Created by ANDRE LUIZ TONON on 21/12/21.
//
import UIKit

protocol BuildView: UIView {
    func buildHierarchy()
    func buildConstraint()
    func buildConfigure()
    func setupView()
}


extension BuildView {
    func setupView(){
        buildConfigure()
        buildHierarchy()
        buildConstraint()
    }
    
    func buildConfigure() { }
}
