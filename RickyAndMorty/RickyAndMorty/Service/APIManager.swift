//
//  APIManager.swift
//  RickyAndMorty
//
//  Created by Alexandre Cardoso on 13/01/22.
//

import Foundation

class APIManager {
	
	func fetchCharacter(url: URL, completion: @escaping(Character?) -> Void) {
		
		let dataTask = URLSession.shared.dataTask(with: url) { data, response, error in
			
			if let _ = error, data == nil {
				completion(nil)
			}
			
			if let response = response, let status = response as? HTTPURLResponse {
				if status.statusCode != 200 {
					completion(nil)
				}
			}
			
			guard let data = data else {
				return completion(nil)
			}
			
			do {
				let result = try JSONDecoder().decode(Character.self, from: data)
				completion(result)
			} catch let error {
				print("Erro JSONDecoder: \(error.localizedDescription)")
				completion(nil)
			}
			
		}
		
		dataTask.resume()
		
	}
	
}
