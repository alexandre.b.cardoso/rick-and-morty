//
//  Character.swift
//  RickyAndMorty
//
//  Created by Alexandre Cardoso on 13/01/22.
//

import Foundation

struct Character: Decodable {
	let info: Info
	let results: [Result]
}

struct Info: Decodable {
	let count: Int
	let pages: Int
	let next: String?
	let prev: String?
}

struct Result: Decodable {
	let id: Int
	let name: String
	let status: Status
	let species: String
	let type: String
	let gender: Gender
	let origin: Origin
	let location: Location
	let image: String
	let episode: [String]
	let url: String
	let created: String
}

struct Origin: Decodable {
	let name: String
	let url: String
}

struct Location: Decodable {
	let name: String
	let url: String
}

enum Status: String, Decodable {
	case alive = "Alive"
	case dead = "Dead"
	case unknown = "unknown"
}

enum Gender: String, Decodable {
	case female = "Female"
	case male = "Male"
	case genderless = "Genderless"
	case unknown = "unknown"
}
