//
//  HomeViewController.swift
//  RickyAndMorty
//
//  Created by ANDRE LUIZ TONON on 21/12/21.
//

import UIKit

class HomeViewController: UIViewController {
	
	lazy var viewModel: HomeViewModel = .init(delegate: self)
    
    override func loadView() {
        let homeview = HomeView()
        homeview.delegate = self
        self.view = homeview
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configNavigationController()
        view.backgroundColor = .white
		  loadData()
    }
	
	private func loadData() {
		viewModel.loadCharacter()
	}
    
    private func configNavigationController() {
        title = "Characters"
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
}

extension HomeViewController: HomeViewDelegate {
	
    func getCountElement() -> Int {
		 return viewModel.numberOfRows
    }
    
	func getCharacter(indexPath: IndexPath) -> Result? {
		viewModel.getCharacter(indexPath: indexPath)
	}
	    
}


extension HomeViewController: HomeViewModelDelegate {

	func successCharacter() {
		DispatchQueue.main.async {
			if let view = self.view as? HomeView {
				view.reloadData()
			}
		}
	}

}
