//
//  HomeViewModel.swift
//  RickyAndMorty
//
//  Created by Alexandre Cardoso on 13/01/22.
//

import Foundation
import SwiftUI

protocol HomeViewModelDelegate: AnyObject {
	func successCharacter()
}

class HomeViewModel {
	
	// MARK: - Properties
	var character: Character? = nil
	let service = APIManager()
	var numberOfRows: Int {
		return character?.results.count ?? 0
	}
	private weak var delegate: HomeViewModelDelegate?
	
	init(delegate: HomeViewModelDelegate?) {
		self.delegate = delegate
	}
	
	// MARK: - Method
	func loadCharacter() {
		guard let url = URL(string: "https://rickandmortyapi.com/api/character")
		else { return }

		service.fetchCharacter(url: url) { success in
			if let success = success {
				print("Sucesso API")
				self.character = success
				self.delegate?.successCharacter()
			} else {
				print("Erro API")
			}
		}
	}
	
	func getCharacter(indexPath: IndexPath) -> Result? {
		return character?.results[indexPath.row] ?? nil
	}
	
}
