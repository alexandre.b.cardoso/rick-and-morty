//
//  CharacterViewModel.swift
//  RickyAndMorty
//
//  Created by Alexandre Cardoso on 09/01/22.
//

import Foundation

protocol CharacterViewModelDelegate {
	var name: String { get }
	var episodeCount: String { get }
	var statusCharacter: Status { get }
	var avatarString: String { get }
}

struct CharacterViewModel: CharacterViewModelDelegate {
	let name: String
	let episodeCount: String
	let statusCharacter: Status
	let avatarString: String
	
	init(name: String, episodeCount: Int, statusCharacter: Status, avatarString: String) {
		self.name = name
		self.episodeCount = String(episodeCount)
		self.statusCharacter = statusCharacter
		self.avatarString = avatarString
	}
}
