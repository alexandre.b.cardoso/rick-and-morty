//
//  CharacterTableViewCell.swift
//  RickyAndMorty
//
//  Created by Alexandre Cardoso on 09/01/22.
//

import UIKit
import SwiftUI

class CharacterTableViewCell: UITableViewCell {
	
	static let identifier = String(describing: CharacterTableViewCell.self)
	
	internal var viewModel: CharacterViewModelDelegate? {
		didSet {
			update()
		}
	}
	
	// MARK: - Element UI
	
	private let mainView: UIView = {
		let view = UIView()
		view.translatesAutoresizingMaskIntoConstraints = false
		view.layer.cornerRadius = 10
		view.layer.borderWidth = 0.5
		view.layer.borderColor = UIColor.darkGray.cgColor
		view.clipsToBounds = true
		view.backgroundColor = .systemBackground
		return view
	}()
	
	private let statusView: UIView = {
		let view = UIView()
		view.translatesAutoresizingMaskIntoConstraints = false
		view.backgroundColor = .green
		return view
	}()
	
	private let avatarImageView: UIImageView = {
		let imageView = UIImageView()
		imageView.translatesAutoresizingMaskIntoConstraints = false
		imageView.backgroundColor = .lightGray
		imageView.layer.cornerRadius = 40
		imageView.clipsToBounds = true
		imageView.contentMode = .scaleAspectFill
		return imageView
	}()
	
	private lazy var mainStackView: UIStackView = {
		let stackView = UIStackView(arrangedSubviews: [
			nameLabel, episodeCountLabel
		])
		stackView.translatesAutoresizingMaskIntoConstraints = false
		stackView.axis = .vertical
		stackView.alignment = .leading
		stackView.distribution = .fillProportionally
		return stackView
	}()
	
	private let nameLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.textColor = .black
		label.textAlignment = .left
		label.numberOfLines = 1
		label.font = .systemFont(ofSize: 25, weight: .bold)
		return label
	}()
	
	private let episodeCountLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.textColor = .black
		label.textAlignment = .left
		label.numberOfLines = 1
		label.font = .systemFont(ofSize: 17, weight: .light)
		return label
	}()
	
	// MARK: - Initialize
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupView()
	}
		
	@available(*, unavailable)
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	// MARK: - Method private
	
	private func update() {
		guard let viewModel = viewModel else { return }
		
		nameLabel.text = viewModel.name
		episodeCountLabel.text = "\(viewModel.episodeCount) Episode(s)"
		
		switch viewModel.statusCharacter {
			case .alive:
				statusView.backgroundColor = .green
			case .dead:
				statusView.backgroundColor = .red
			case .unknown:
				statusView.backgroundColor = .darkGray
		}
		
		setAvatarImage(urlString: viewModel.avatarString)
	}
	
	private func setAvatarImage(urlString: String) {
		guard let url = URL(string: urlString) else { return }
		
		DispatchQueue.global().async {
			do {
				let data = try Data(contentsOf: url)
				let image = UIImage(data: data)
				
				DispatchQueue.main.async {
					self.avatarImageView.image = image
				}
				
			} catch  { }
			
		}
	}


}

// MARK: - Extension BuildView

extension CharacterTableViewCell: BuildView {
	
	func buildConfigure() {
		contentView.backgroundColor = .white
	}
	
	func buildHierarchy() {
		mainView.addSubview(statusView)
		mainView.addSubview(avatarImageView)
		mainView.addSubview(mainStackView)
		contentView.addSubview(mainView)
	}
	
	func buildConstraint() {
		
		NSLayoutConstraint.activate([
			mainView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
			mainView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
			mainView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
			mainView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant: -5),
			
			statusView.topAnchor.constraint(equalTo: mainView.topAnchor),
			statusView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
			statusView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor),
			statusView.widthAnchor.constraint(equalToConstant: 10),
			
			avatarImageView.centerYAnchor.constraint(equalTo: mainView.centerYAnchor),
			avatarImageView.leadingAnchor.constraint(equalTo: statusView.trailingAnchor, constant: 5),
			avatarImageView.heightAnchor.constraint(equalToConstant: 80),
			avatarImageView.widthAnchor.constraint(equalToConstant: 80),
			
			mainStackView.leadingAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: 10),
			mainStackView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -20),
			mainStackView.centerYAnchor.constraint(equalTo: avatarImageView.centerYAnchor),
		])
	}
	
}
