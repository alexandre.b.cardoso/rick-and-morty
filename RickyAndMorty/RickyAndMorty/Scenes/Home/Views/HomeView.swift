//
//  HomeView.swift
//  RickyAndMorty
//
//  Created by ANDRE LUIZ TONON on 21/12/21.
//

import UIKit

protocol HomeViewDelegate: AnyObject {
    func getCountElement() -> Int
    func getCharacter(indexPath: IndexPath) -> Result?
}

final class HomeView: UIView {
    
    private lazy var tableview: UITableView = {
        let tbv = UITableView()
        tbv.translatesAutoresizingMaskIntoConstraints = false
        tbv.delegate = self
        tbv.dataSource = self
		  tbv.rowHeight = 100
		  tbv.separatorStyle = .none
		  tbv.register(
			CharacterTableViewCell.self,
			forCellReuseIdentifier: CharacterTableViewCell.identifier)
        return tbv
    }()
    
	weak var delegate: HomeViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
	
	func reloadData() {
		self.tableview.reloadData()
	}
	
	private func cellCharacter(
		_ character: Result,
		_ tableView: UITableView,
		cellForRowAt indexPath: IndexPath
	) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(
			withIdentifier: CharacterTableViewCell.identifier,
			for: indexPath) as? CharacterTableViewCell
		else {
			return UITableViewCell()
		}
		
		let viewModel = CharacterViewModel(
			name: character.name,
			episodeCount: character.episode.count,
			statusCharacter: character.status,
			avatarString: character.image
		)
		cell.viewModel = viewModel
		
		return cell
	}
	
}

// MARK: - Extension TableViewDelegate

extension HomeView: UITableViewDelegate { }

// MARK: - Extension TableViewDataSource

extension HomeView: UITableViewDataSource {
	
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.getCountElement() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		  guard let character = delegate?.getCharacter(indexPath: indexPath) else {
			  return UITableViewCell()
		  }
		 
		 let cell = cellCharacter(character, tableView, cellForRowAt: indexPath)
        
        return cell
    }
    
}

// MARK: - Extension BuildView

extension HomeView: BuildView {
	
    func buildHierarchy() {
        addSubview(tableview)
    }
    
    func buildConstraint() {
		 
        NSLayoutConstraint.activate([
            tableview.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            tableview.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            tableview.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
				tableview.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
		 
    }
    
}
