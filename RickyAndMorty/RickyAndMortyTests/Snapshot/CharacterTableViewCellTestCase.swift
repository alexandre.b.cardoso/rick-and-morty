//
//  CharacterTableViewCellTestCase.swift
//  RickyAndMortyTests
//
//  Created by Alexandre Cardoso on 09/01/22.
//

import XCTest
import SnapshotTesting
@testable import RickyAndMorty

class CharacterTableViewCellTestCase: XCTestCase {
	
	var sut: CharacterTableViewCell!

	override func setUp() {
		super.setUp()
		sut = CharacterTableViewCell(style: .default, reuseIdentifier: nil)
	}
	
	override func tearDown() {
		sut = nil
		super.tearDown()
	}
	
	func testCharacterCell() {
		let viewModel = CharacterViewModel(name: "Rick Sanchez", episodeCount: "41")
		sut.viewModel = viewModel
		
		assertSnapshot(matching: sut, as: .image, record: false)
	}

}
